function loadPage(aName) {
    r = new XMLHttpRequest();
    r.onreadystatechange = function () {
        if (this.readyState == 4) {
            alert("Status :" + this.status + " Data :" + this.responseXML);
            var e = document.getElementById("content");
            if (e)
                e.innerHTML = this.responseXML;
        }
    }
    r.responseType = "document";
    r.open("GET", aName, true);
    r.send();
}